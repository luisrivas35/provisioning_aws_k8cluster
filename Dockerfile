FROM ubuntu:22.04
# Actualiza el sistema y instala los paquetes necesarios
WORKDIR /app
RUN apt-get update 
RUN apt-get upgrade -y 
RUN apt-get install -y curl wget unzip 
RUN apt-get install -y git 
RUN apt-get install python3 python3-pip -y
RUN git clone https://github.com/kubernetes-incubator/kubespray.git && \
    cd kubespray && \
    pip install -r requirements.txt
WORKDIR /app
COPY . . 
RUN chmod 400 clase_key.pem
# Ejecuta el contenedor en modo interactivo
ENTRYPOINT [ "" ]
CMD ["/bin/bash"]
